$( function() {
  /*
  $('.js-form-type').on('click', function () {
    var $tab = $(this),
        $form = $('#' + $tab.data('form') + 'Form');

    $('.js-sign-up-form').addClass('hide');
    $form.removeClass('hide');
    $tab.addClass('active').siblings().removeClass('active');
  });
  */

  $('#signUpBtn, #loginBtn, #getEmailVerification').on('click', function() {
    var $submitBtn = $(this),
        $form = $submitBtn.closest('form'),
        isFormVaid = pe.helper.validateForm($form);

    if (isFormVaid) {
      $.ajax({
        url: $form.attr('action'),
        type: $form.attr('method'),
        data: $form.serialize(),
        success: function(data) {
          var data = JSON.parse(data);
          
          if (data.status && $form.hasClass('js-login-form')) {
            location.href = '/user/profile';
          }
          pe.helper.flash(data.flash);
          console.log(data);
        }
      });
    }
  });

  /*
  $('#loginBtn').on('click', function() {
    var $submitBtn = $(this),
        $form = $submitBtn.closest('form'),
        isFormVaid = pe.helper.validateForm($form);

    if (isFormVaid) {
      $.ajax({
        url: $form.attr('action'),
        type: $form.attr('method'),
        data: $form.serialize(),
        success: function(data, status) {
          var data = JSON.parse(data);

          pe.helper.flash(data.flash);
          console.log(data);
        }
      });
    }
  });

  $('#getEmailVerification').on('click', function() {
    var $submitBtn = $(this),
        $form = $submitBtn.closest('form'),
        isFormVaid = pe.helper.validateForm($form);

    if (isFormVaid) {
      $.ajax({
        url: $form.attr('action'),
        type: $form.attr('method'),
        data: $form.serialize(),
        success: function(data, status) {
          var data = JSON.parse(data);

          pe.helper.flash(data.flash);
          console.log(data);
        }
      });
    }
  });
  */
 
  $('#forgotPass').on('click', function(e) {
    e.preventDefault();
    $('#forgotPassForm').toggleClass('hide');
  });
});