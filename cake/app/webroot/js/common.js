$(function () {
  "use strict";

  $('#prescriptionFile').on('change', function(e) {
    var $bidderInput = $(this),
        file = document.getElementById('prescriptionFile').files[0],
        reader = new FileReader(),
        fileTypesAllowed = ['image/jpg','image/jpeg','image/png', 'application/pdf'];

    

    if (file && fileTypesAllowed.indexOf(file.type) !== -1 && file.size < 1048576) {
      showHideAttention(0);
    } else {
      showHideAttention(1);
    }
  });

  $('#uploadPrescriptionFile').on('click', function() {
    var file = document.getElementById('prescriptionFile').files[0],
        formData = new FormData(),
        isFormValid = pe.helper.validateForm($(this).closest('form'));
        
    if (isFormValid) {
      formData.append('file', file);
      formData.append('description', $('#prescriptionDesc').val());
      $.ajax({
        url: '/user/upload',
        type: 'POST',
        data: formData,
        contentType: false,
        processData: false,
        success : function(data) {
          var data = JSON.parse(data),
              status = data.status;
          
          if (!status) {
            pe.helper.flash();
          } else {
            location.reload();
          }
        }  
      });
    }
  });

  function showHideAttention(show) {
    if (show) {
      $('.js-invalid-file').removeClass('hide');
      $('#uploadPrescriptionFile').addClass('disabled');
      $('#prescriptionFile').attr('value', '');
    } else {
      $('.js-invalid-file').addClass('hide');
      $('#uploadPrescriptionFile').removeClass('disabled');
    }
  }

  $('#shareInput').on('keyup click', pe.helper.debounce(function() {
    var key = $(this).val(),
        $form = $(this).closest('form');

    $.ajax({
      url: '/user/searchUser',
      type: 'post',
      data: {'key' : key, 'assetId' : $('#shareAssetId').val()},
      success : function(data) {
        var data = JSON.parse(data),
            status = data.status,
            html = data.resultHtml;

        $('#searchResultDiv').html(html);
        // console.log(data);
      }  
    });
  }, 250));

  $('#searchResultDiv').on('click', '.js-user-search-item', function() {
    var $el = $(this);

    $('#shareInput').val($el.text());
    $('#shareUserId').val($el.data('userId'));
    $('.js-search-dropdown').remove();
    shareBtnStatus();
  });

  $('#shareAssetBtn').on('click', function() {
    var $btn = $(this),
        $form = $btn.closest('form');

    $('.js-share-success').addClass('hide');
    $('.js-share-error').addClass('hide');
    if ($('#shareAssetId').val() && $('#shareAssetId').val()) {
      $.ajax({
        url: $form.attr('action'),
        type: $form.attr('method'),
        data: $form.serialize(),
        success : function(data) {
          var data = JSON.parse(data),
              status = data.status;

          if (status) {
            $('.js-share-success').removeClass('hide');
          } else {
            $('.js-share-error').removeClass('hide');
          }
        }  
      });
    } else {
      $('.js-share-error').removeClass('hide');
    }
  })

  function shareBtnStatus() {
    if ($('#shareAssetId').val() && $('#shareAssetId').val()) {
      $('#shareAssetBtn').removeClass('disabled');
    } else {
      $('#shareAssetBtn').addClass('disabled');
    }
  }

  $('#closeModal').on('click', function() {
    $('.js-form-share-modal').modal("hide");
  });

  $('body').on('click', '.js-share-prescription', function() {
    var $el = $(this),
        assetId = $el.data('assetId');

    $('#shareAssetId').val(assetId);    
    $('#shareAssetTopId').text(assetId);
    $('.js-form-share-modal').modal('show');
  })
});