var pe = {};
pe.helper = {};

pe.helper.flash = function(flash = {}, callback) {
  var type = flash.type || 'warning',
      msg = flash.msg || 'Something went wrong',
      linkText = flash.linkText || 'Go to home',
      redirect = flash.linkText ? flash.redirect : '/',
      html = '<div class="js-pe-flash alert alert-' + type + ' alert-dismissable">\
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>\
        <strong>' + pe.helper.capitalizeFirstLetter(type) + '! </strong>' + msg + '\
        <a class="link-flash" href="' + redirect + '">' + linkText + '</a>\
      </div>';
      
  $('.js-pe-flash').remove();
  $('.login').prepend(html);
};

pe.helper.capitalizeFirstLetter = function(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
};

pe.helper.validateForm = function (form) {
  var $form = $(form),
      $firstName = $form.find('#firstName'),
      $lastName = $form.find('#lastName'),
      $email = $form.find('#email'),
      $roles = $form.find('#roles'),
      $password = $form.find('#password'),
      $confirmPassword = $form.find('#confirmPassword'),
      $prescriptionDesc = $form.find('#prescriptionDesc'),
      $mobile = $form.find('#mobile'),
      nameRegEx = /^[a-zA-Z\s]+$/i,
      emailRegEx = /^[a-z0-9_\.-]+\@[\da-z\.-]+\.[a-z\.]{2,6}$/,
      phoneRegEx = /^[7-9]{1}[0-9]{9}$/,
      passwordRegEx = /^\S{8,}$/,
      isFormValid = true;

  if ($firstName.length) {
    if (!nameRegEx.test($firstName.val())) {
      $firstName.addClass('error');
      isFormValid =false;
    } else {
      $firstName.removeClass('error');
    }
  }

  if ($prescriptionDesc.length) {
    if (!$prescriptionDesc.val()) {
      $prescriptionDesc.addClass('error');
      isFormValid =false;
    } else {
      $prescriptionDesc.removeClass('error');
    }
  }
  
  if ($lastName.length) {
    if (!nameRegEx.test($lastName.val())) {
      $lastName.addClass('error');
      isFormValid =false;
    } else {
      $lastName.removeClass('error');
    }
  }

  if ($email.length) {
    if (!emailRegEx.test($email.val())) {
      $email.addClass('error');
      isFormValid =false;
    } else {
      $email.removeClass('error');
    }
  }

  if ($mobile.length) {
    if ($mobile && !phoneRegEx.test($mobile.val())) {
      $mobile.addClass('error');
      isFormValid =false;
    }
  }

  if ($roles.length) {
    if (!$roles.val()) {
      $roles.addClass('error');
      isFormValid =false;
    } else {
      $roles.removeClass('error');
    }
  }

  if ($confirmPassword.length) {
    if (!passwordRegEx.test($password.val()) || !passwordRegEx.test($confirmPassword.val()) || $confirmPassword.val() !== $password.val()) {
      $confirmPassword.addClass('error');
      $password.addClass('error');
      isFormValid =false;
    } else {
      $confirmPassword.removeClass('error');
      $password.removeClass('error');
    }
  }

  if ($password.length) {
    if (!passwordRegEx.test($password.val())) {
      $password.addClass('error');
      isFormValid =false;
    } else {
      $password.removeClass('error');
    }
  }

  return isFormValid;
}

pe.helper.redirect = function(obj={}) {
  if (obj.redirect) {
    location.href = redirect;
  }
}

// Returns a function, that, as long as it continues to be invoked, will not
// be triggered. The function will be called after it stops being called for
// N milliseconds. If `immediate` is passed, trigger the function on the
// leading edge, instead of the trailing.
pe.helper.debounce =  function (func, wait, immediate) {
  var timeout;
  return function() {
    var context = this, args = arguments;
    var later = function() {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };
    var callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
};