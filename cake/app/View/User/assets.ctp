<div class="container">
  <?php if (count($assets)) { ?>
    <h2></h2>
    <table class="tab-pane table table-striped">
      <tr>
        <th>Asset ID</th>
        <th>Description</th>
        <th>Shared At</th>
        <th>Shared by</th>
      </tr>
      <?php foreach($assets as $p) { ?>
        <tr>
          <td><?= $p['id']; ?></td>
          <td><a target="_blank" href="<?= $p['link']; ?>"><?= $p['description']; ?></a></td>
          <td><?= $p['created_at']; ?></td>
          <td><?= $p['shared_by']; ?></td>
      <?php } ?>
    </table>
  <?php } else { ?>
    <p>No assets found</p>
  <?php } ?>
</div>