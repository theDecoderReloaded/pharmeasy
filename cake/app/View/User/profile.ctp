<div class="container">
  <div class="row">
    <div class="col-xs-12">
      <h1>Upload new prescriptions</h1>
      <form method="POST" enctype="multipart/form-data">
        <p>
          File accepted: .jpeg .jpg .png .pdf <br/>Max file size: 1MB
        </p>
        <div class="form-group">
          <input type="file" class="form-control" id="prescriptionFile" name="file">
        </div>
        <div class="form-group">
          <textarea type="textarea" class="form-control" name="description" id="prescriptionDesc" placeholder="Enter description"></textarea>
        </div>
        <div class="form-group">
          <button type="button" class="btn btn-primary btn-margin disabled" id="uploadPrescriptionFile" >Upload</button>
        </div>
      </form>
      <?= $this->element('prescriptions_table'); ?>
    </div>    
  </div>
  <?= $this->element('share_modal'); ?>
</div>
