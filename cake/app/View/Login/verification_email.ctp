<section class="login">
  <img src="<?= $peLogo ?>"/>
  <form class="sign-up-form" method="post" id="emailVerificationForm" action="/login/verificationEmail">
    <div class="form-group">
      <input class="form-input" type="text" name="user[email]" id="email" placeholder="Email"/>
      <i class="fa fa-envelope input-icon"></i>
    </div>
    <button type="button" id="getEmailVerification" class="btn btn-nm btn-green signup-login-btn">Generate Email</button>
  </form>
</section>