<?php $ifVerified = !empty($ifVerified) ? $ifVerified : false; ?>
<div class="container">
  <?php if ($ifVerified) { ?>
    <div class="alert alert-success alert-dismissable">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <strong>Voila! You have been verified successfully.</strong>
      <a class="link-flash" href="/login">login</a>
    </div>
  <?php } else { ?>
    <div class="alert alert-warning alert-dismissable">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <strong>Alas! Invalid link. Generate verification link again.</strong>
      <a class="link-flash" href="/login/verificationEmail">generate verification link</a>
    </div>
  <?php } ?>
</div>  