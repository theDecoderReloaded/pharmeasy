<section class="login">
  <img src="<?= $peLogo ?>"/>
  <ul class="users-tab">
    <li class="form-type js-form-type active">Sign Up</li>
    <li class="form-type js-form-type"><a href="/login">Login</a></li>
  </ul>
  <form class="sign-up-form js-sign-up-form" method="post" id="signUpForm" action="/login/signUp">
    <div class="form-group">
      <input class="form-input" type="text" name="user[name]" id="firstName" placeholder="Full Name"/>
      <i class="fa fa-user input-icon"></i>
    </div>
    <div class="form-group">
      <input class="form-input" type="text" name="user[email]" id="email" placeholder="Email"/>
      <i class="fa fa-envelope input-icon"></i>
    </div>
    <div class="form-group">
      <!-- <input class="form-input" type="text" id="roles" placeholder="Role"/>
      <input type="hidden" name="user[role_id]" > -->
      <select class="form-input" name="user[role_id]" id="roles">
        <option disabled selected>Role</option>
        <?php foreach($roles as $roleId => $roleName) { ?>
          <option value="<?= $roleId ?>"><?= $roleName ?></option>
        <?php } ?>
      </select>
      <i class="fa fa-user-secret input-icon"></i>
      <!-- <ul class="user-roles js-user-roles">
        <?php foreach($roles as $roleId => $roleName) { ?>
          <li class="role-item js-role-item" data-role-id="<?= $roleId ?>"><?= $roleName ?></li>
        <?php } ?>
      </ul> -->
    </div>
    <div class="form-group">
      <input class="form-input" type="password" name="user[password]" id="password" placeholder="Password"/>      
      <i class="fa fa-lock input-icon"></i>
    </div>
    <div class="form-group">
      <input class="form-input" type="password" id="confirmPassword" placeholder="Confirm Password"/>      
      <i class="fa fa-lock input-icon"></i>
    </div>
    <button type="button" id="signUpBtn" class="btn btn-nm btn-green signup-login-btn">Sign Up</button>
  </form>
</section>