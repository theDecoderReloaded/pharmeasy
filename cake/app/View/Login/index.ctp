<section class="login">
  <img src="<?= $peLogo ?>"/>
  <ul class="users-tab">
    <li class="form-type js-form-type"><a href="/login/signUp">Sign Up</a></li>
    <li class="form-type js-form-type active">Login</li>
  </ul>
  <form class="sign-up-form js-sign-up-form js-login-form" method="post" id="loginForm" action="/login">
    <div class="form-group">
      <input class="form-input" type="text" name="user[email]" id="email" placeholder="Email"/>
      <i class="fa fa-envelope input-icon"></i>
    </div>
    <div class="form-group">
      <input class="form-input" type="password" name="user[password]" id="password" placeholder="Password"/>
      <i class="fa fa-lock input-icon"></i>
    </div>
    <a class="forgot-pass" id="forgotPass" href="">Forgot Password?</a>
    <button type="button" id="loginBtn" class="btn btn-nm btn-green signup-login-btn">Log In</button>
  </form>
  <form class="forgot-pass-form hide" method="post" id="forgotPassForm" action="forgotPassword">
    <p class="para">Enter your email to get a password reset link on you email</p>
    <div class="form-group">
      <input class="form-input" type="text" name="user[email]" id="emailUser" placeholder="Email"/>
      <i class="fa fa-envelope input-icon"></i>
    </div>
    <button type="button" id="forgotPasswordBtn" class="btn btn-nm btn-green forgot-password-btn">Submit</button>
  </form>
</section>