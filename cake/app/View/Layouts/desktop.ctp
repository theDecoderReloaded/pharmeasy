<!DOCTYPE html>
<html lang="en">
<head>
  <?php
    $this->Html->css([
      'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css',
      'font_awesome.css',
      ], ['inline' => false]);
    $this->Html->css([      
      'reset',
      'common',
      'login',
      ], ['inline' => false]);
    $this->Html->script([
        'https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js',        
        'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js',
        ], ['inline' => false, 'block' => 'vendorjs']);
  ?>
  <?= $this->Html->charset(); ?>
  <title><?= isset($pageTitle) ? $pageTitle : 'Pharm Easy'; ?></title>
  <meta name="viewport" content="width=device-width" />
  <?php 
    echo $this->fetch('meta');
    echo $this->fetch('css');
    echo $this->fetch('vendorjs');
  ?>
</head>
  <body class="body">
    <?= $this->element('header'); ?>
    <?= $this->Session->flash(); ?>
    <?= $this->fetch('content'); ?> 

    <?php 
      $this->Html->script([
        'helper',
        'common',
        'login',
        ], ['inline' => false]);

      echo $this->fetch('script'); 
    ?> 
  </body>
</html>
