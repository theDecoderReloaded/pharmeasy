<h2>Your Prescriptions</h2>
<?php if (count($prescriptions)) { ?>
  <table class="tab-pane table table-striped">
    <tr>
      <th>Prescription ID</th>
      <th>Description</th>
      <th>Uploaded At</th>
      <th>Shared with</th>
      <th>Actions</th>
    </tr>
    <?php foreach($prescriptions as $p) { ?>
      <tr>
        <td><?= $p['id']; ?></td>
        <td><a target="_blank" href="<?= $p['link']; ?>"><?= $p['description']; ?></a></td>
        <td><?= $p['created_at']; ?></td>
        <td><?= $p['shared_with'] ? $p['shared_with'] : 'Not yet shared with anyone'; ?></td>
        <td>
          <button class="btn btn-primary js-share-prescription" 
            data-asset-id="<?= $p['id']; ?>">Share with more</button>
        </td>
      </tr>
    <?php } ?>
  </table>
<?php } else { ?>
  <p>No prescriptions found</p>
<?php } ?>