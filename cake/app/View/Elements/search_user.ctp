<ul class="search-dropdown js-search-dropdown">
  <?php if (count($users)) { ?>
    <?php foreach ($users as $id => $name) { ?>  
      <li class="js-user-search-item user-search-item" data-user-id="<?= $id ?>"><?= $name ?></li>
    <?php } ?>
  <?php } else { ?>
    <li class="user-search-item">No results found</li>
  <?php } ?>
</ul>
