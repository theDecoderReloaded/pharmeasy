<?php 
  $user = $this->Session->read('user'); 
?>
<nav class="navbar navbar-default">
  <div class="container">
    <div class="navbar-header">
      <a class="navbar-brand" href="/">PharmEasy</a>
    </div>
    <ul class="nav navbar-nav">
      <?php if ($isUserLoggedIn) { ?>
        <li class="<?= $this->action == 'profile' ? 'active' : '' ?>"><a href="/user/profile"><?= "Your prescriptions"; ?></a></li>
        <li class="<?= $this->action == 'assets' ? 'active' : '' ?>"><a href="/user/assets" ><?= "Your assets"; ?></a></li>
      <?php } ?>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <?php if ($isUserLoggedIn) { ?>
        <li><a href="#"><?= "{$user['name']} ({$user['role_name']})"; ?></a></li>
      <?php } ?>
      <li>
        <a href="<?= $isUserLoggedIn ? '/login/logout' : '/login' ?>">
          <?= $isUserLoggedIn ? 'Logout' : 'Login/Singup'?>
        </a>
      </li>
    </ul>
  </div>
</nav>