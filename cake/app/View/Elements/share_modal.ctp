<div class="modal fade js-form-share-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4>Share asset id <span id="shareAssetTopId"></span> with anyone</h4>
      </div>
      <div class="modal-body">
        <form class="share-form js-share-form" method="post" id="shareForm" action="/user/share">
          <p class="js-share-error share-error hide">Some error occured! Please try again.</p>
          <p class="js-share-success share-success hide">File shared suseefully! Continue sharing with more or <a href="/user/profile">reload</a>.</p>
          <input type="hidden" name="asset[user_asset_id]" id="shareAssetId"/>
          <input type="hidden" name="asset[user_id]" id="shareUserId"/>
          <div class="form-group no-bottom-margin">
            <input class="form-input" type="text" id="shareInput" placeholder="Share with"/>
            <i class="fa fa-search input-icon"></i>
          </div>
          <div class="search-result" id="searchResultDiv"></div>
          <div class="form-group text-right">
            <button type="button" class="btn btn-primary btn-margin disabled" id="shareAssetBtn" >Share</button>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button id="closeModal" class="btn btn-default">Cancel</button>
      </div>
    </div>
  </div>
</div>