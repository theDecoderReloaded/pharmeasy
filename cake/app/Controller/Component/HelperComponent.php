<?php
App::uses('UtilityFunctions', 'Lib');
App::uses('Security', 'Utility');
App::uses('CakeEmail', 'Network/Email');

class HelperComponent extends Component {
  
  public function password($password) {
    return Security::hash($password, null, true);
  }

  public function comparePassword($dbPass, $reqPass) {
    return $this->password($reqPass) === $dbPass;
  }

  public function getHash($userKey) {
    return Security::hash($userKey . microtime(), 'sha256', true);
  }

  public function sendEmail($params) {
    $Email = new CakeEmail('gmail');
    if (!empty($params['to']) && !empty($params['subject']) && !empty($params['msg'])) {
      $Email->to($params['to']);
      $Email->subject($params['subject']);
      return $Email->send($params['msg']);
    }
    return false;
  }

  public function getTimeDiff($date2, $date1 = false) {
    $date1 = !$date1 ? time() : strtotime($date1);
    $diffInSec = $date1 - strtotime($date2);
    return [
      'days' => (int) $diffInSec / (24 * 60 * 60),
      'hours' => (int) $diffInSec / (60 * 60),
      'minutes' => (int) $diffInSec / (60),
      'weeks' => (int) $diffInSec / (24 * 60 * 60 * 7),
    ];
  }
}