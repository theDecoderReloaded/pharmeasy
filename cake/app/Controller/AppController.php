<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

  public $components = array('Session', 'Cookie', 'DebugKit.Toolbar', 'Helper');
  public $layout;
  public $isUserLoggedIn = false;

  public function beforeFilter() {
    $this->includeLayout();
    $this->setGlobalVars();
  }

  /**
   * Include mobile layout if the request is page load
   * 
   */
  private function includeLayout() {
    if (!$this->request->is('ajax') && $this->request->is('get') && $this->name !== 'CakeError') {
      $this->layout = 'desktop';
    } else {
      $this->layout = false;
    }
  }

  private function setGlobalVars() {
    $this->isUserLoggedIn = (bool) $this->Session->read('user');
    $this->set('isUserLoggedIn', $this->isUserLoggedIn);
    $this->set('peLogo', Configure::read('PE.logo'));
  }

  protected function buildSession($user) {
    $this->loadModel('Role');
    $roles = $this->Role->getActiveRole();
    $user['role_name'] = $roles[$user['role_id']];
    $this->Session->write('user', $user);
  }
}

