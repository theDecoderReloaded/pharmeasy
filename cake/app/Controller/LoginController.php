<?php
App::uses('Security', 'Utility');

class LoginController extends AppController {

  public $responseDict = [
    'EMAIL_DOESNOT_EXIST' => ['status' => false, 'flash' => ['msg' => 'Email id does not exists', 'redirect' => '/login/signUp', 'linkText' => 'signUp', 'type' => 'warning']],
    'EMAIL_NOT_VERIFIED' => ['status' => false, 'flash' => ['msg' => 'Email not verified. No issues, we will again send the verification link', 'type' => 'warning', 'linkText' => 'get verification link', 'redirect' => '/login/verificationEmail']],
    'WRONG_CREDS' => ['status' => false, 'flash' => ['msg' => 'Email or password wrong', 'type' => 'danger']],
    'EMAIL_ALREADY_EXISTS' => ['status' => false, 'flash' => ['msg' => 'Email id already in use', 'redirect' => '/login', 'linkText' => 'login', 'type' => 'warning']],
    'KEY_NOT_FOUND' => ['status' => false, 'flash' => ['msg' => 'Some error occured, try again', 'redirect' => '/login', 'linkText' => 'login', 'type' => 'warning']],
    'VERIFICATION_LINK_SENT' => ['status' => true, 'flash' => ['msg' => 'We have sent an verification link to your email.', 'redirect' => '/login', 'linkText' => 'login', 'type' => 'success']],
    'RESP_SUCCESS' => ['status' => true, 'flash' => ['msg' => 'Success! Operation successful', 'type' => 'success', 'linkText' => '/user/profile', 'Go to profile']],
    'RESP_FAIL' => ['status' => false, 'flash' => ['msg' => 'Failed! Operation failed', 'redirect' => '/login', 'linkText' => 'login', 'type' => 'success']],
  ]; 

  public function index() {
    if ($this->request->is('post') && $this->request->is('ajax')) {
      $reqUser = $this->request->data('user');
      $response = $this->responseDict['RESP_FAIL'];
      if (!empty($reqUser['email']) && !empty($reqUser['password'])) {
        $this->loadModel('User');
        $dbUser = $this->User->findByEmail($reqUser['email']);
        if (!empty($dbUser)) {
          $dbUser = $dbUser['User'];
          if ($this->Helper->comparePassword($dbUser['password'], $reqUser['password'])) {
            if ($dbUser['is_verified']) {
              $this->buildSession($dbUser);
              $response = $this->responseDict['RESP_SUCCESS'];
            } else {
              $response = $this->responseDict['EMAIL_NOT_VERIFIED'];
            }
          } else {
            $response = $this->responseDict['WRONG_CREDS'];
          }
        } else {
          $response = $this->responseDict['EMAIL_DOESNOT_EXIST'];
        }
      }
      $this->set('json', $response);
      $this->render('/Elements/json', 'ajax');
    } else if ($this->request->is('get')) {
      
    }
  }

  public function logout() {
    if ($this->request->is('get')) {
      $this->Session->delete('user');
      $this->redirect('/');
      // $this->set('json', ['status' => true]);
      // $this->render('/Elements/json', 'ajax');
    }
  }

  private function _getVerificationEmail($user) {
    $userEmail = !empty($user['email']) ? $user['email'] : '';
    if (!empty($user['id']) && !empty($user['email'])) {
      $this->loadModel('EmailVerificationKey');
      $verificationKey = $this->EmailVerificationKey->save([
        'user_id' => $user['id'],
        'key' => $this->Helper->getHash($user['email'])
      ]);
    } elseif (!empty($user['email'])) {
      $this->loadModel('User');
      $dbUser = $this->User->findByEmail($user['email']);
      if (!empty($dbUser)) {
        $dbUser = $dbUser['User'];
        $this->loadModel('EmailVerificationKey');
        $verificationKey = $this->EmailVerificationKey->save([
          'user_id' => $dbUser['id'],
          'key' => $this->Helper->getHash($dbUser['email'])
        ]);
      } else {
        return $this->responseDict['EMAIL_DOESNOT_EXIST'];
      }
    }
    if (!empty($verificationKey) && !empty($userEmail)) {
      $verificationKey = $verificationKey['EmailVerificationKey']['key'];
      $link = FULL_BASE_URL . "/login/verifyEmail?key=$verificationKey";
      $email = $this->Helper->sendEmail([
        'msg' => "Please click $link to verify",
        'subject' => 'Email verification link',
        'to' => $userEmail
      ]);
      if ($email) {
        return $this->responseDict['VERIFICATION_LINK_SENT'];
      }
    }
    return $this->responseDict['KEY_NOT_FOUND'];
  }

  public function verifyEmail() {
    if ($this->request->is('get')) {
      $key = $this->request->query('key');
      $ifVerified = false;
      $this->loadModel('EmailVerificationKey');
      $vKey = $this->EmailVerificationKey->findByKey($key);
      if (!empty($vKey)) {
        $vObj = $vKey['EmailVerificationKey'];
        if (!empty($vObj['is_valid'])) {
          $this->EmailVerificationKey->clear();
          $this->EmailVerificationKey->save([
            'id' => $vObj['id'],
            'is_valid' => 0
          ]);
          if ($this->Helper->getTimeDiff($vObj['created_at'])['hours'] < 24) {
            $this->loadModel('User');
            $this->User->clear();
            $this->User->save([
              'id' => $vObj['user_id'],
              'is_verified' => 1
            ]);
            $ifVerified = true;
          }
        }
      }
      $this->set('ifVerified', $ifVerified);
    }
  }

  public function verificationEmail() {
    if ($this->request->is('post') && $this->request->is('ajax')) {
      $user = $this->request->data('user');
      $response = $this->_getVerificationEmail($user);
      $this->set('json', $response);
      $this->render('/Elements/json', 'ajax');
    } elseif ($this->request->is('get')) {
      # code...
    }
  }

  public function signUp() {
    if ($this->request->is('post') && $this->request->is('ajax')) {
      $reqUser = $this->request->data('user');
      $response = ['status' => false];
      if (!empty($reqUser['email'])) {
        $this->loadModel('User');
        $dbUser = $this->User->findByEmail($reqUser['email']);
        if (empty($dbUser)) {
          $reqUser['password'] = $this->Helper->password($reqUser['password']);
          $dbUser = $this->User->addUser($reqUser);
          if (!empty($dbUser)) {
            $response = $this->_getVerificationEmail($dbUser['User']);
          } else {
            $response = $this->responseDict['KEY_NOT_FOUND'];
          }
        } else {
          $response = $this->responseDict['EMAIL_ALREADY_EXISTS'];
        }
      } else {
        $response = $this->responseDict['KEY_NOT_FOUND'];
      }
      $this->set('json', $response);
      $this->render('/Elements/json', 'ajax');
    } else if ($this->request->is('get')) {
      $this->loadModel('Role');
      $this->set('roles', $this->Role->getActiveRole());
    }
  }
}
