<?php

class UserController extends AppController {
  private $user = [];

  private $maxFileSize = 1048576;

  public function beforeFilter() {
    parent::beforeFilter();
    if (!$this->isUserLoggedIn) {
      if ($this->request->is('post') && $this->request->is('ajax')) {
        $this->set('json', ['status' => false, 'redirect' => '/login']);
        $this->render('/Elements/json', 'ajax');
      } else {
        $this->redirect('/login');
      }
      $this->response->send();
      $this->_stop();
    } else {
      $this->user = $this->Session->read('user');
    }
  }

  public function profile() {
    if ($this->request->is('get')) {
      $this->loadModel('UserAsset');
      $this->set('prescriptions', $this->UserAsset->getAllAssetInfo($this->user['id']));
    }
  }

  public function share() {
    if ($this->request->is('post') && $this->request->is('ajax')) {
      $asset = $this->request->data('asset');
      $response = false;

      if ($asset['user_asset_id'] && $asset['user_id']) {
        $asset['is_active'] = 1;
        $this->loadModel('UserAssetShare');
        $response = $this->UserAssetShare->save($asset);
      }
      $this->set('json', ['status' => (bool) $response]);
      $this->render('/Elements/json', 'ajax');
    }
  }

  public function searchUser() {
    if ($this->request->is('post') && $this->request->is('ajax')) {
      $key = $this->request->data('key');
      $assetId = $this->request->data('assetId');

      $this->loadModel('UserAssetShare');
      $excludeUserIds = array_values($this->UserAssetShare->find('list', [
        'fields' => ['id', 'user_id'],
        'conditions' => ['user_asset_id' => $assetId]
      ]));
      $excludeUserIds[] = $this->user['id'];
      $this->loadModel('User');
      $users = $this->User->find('list', [
        'conditions' => [
          'name LIKE' => "%$key%",
          'NOT' => ['id' => $excludeUserIds],
        ],
        'limit' => 5
      ]);
      $this->set('users', $users);
      $newView = new View($this);
      $html = $newView->render('/Elements/search_user', 'ajax');

      $this->set('json', ['status' => true, 'resultHtml' => $html]);
      $this->render('/Elements/json', 'ajax');
    }
  }

  public function assets() {
    if ($this->request->is('get')) {
      $this->loadModel('UserAssetShare');
      $this->set('assets', $this->UserAssetShare->getAssets($this->user['id']));
    }
  }


  public function upload() {
    if ($this->request->is('post') && $this->request->is('ajax')) {
      $response = $this->_uploadAsset($this->request->data('description'));
      $this->set('json', ['status' => (bool) $response]);
      $this->render('/Elements/json', 'ajax');
    } else if ($this->request->is('get')) {

    }
  }

  private function _uploadAsset($fileDesription=null) {
    $prescriptionFile = $_FILES['file'];
    $name     = $prescriptionFile['name'];
    $tmpName  = $prescriptionFile['tmp_name'];
    $extension = pathinfo($name, PATHINFO_EXTENSION);
    $newname = $this->Helper->getHash($name) . ".$extension";
    if ($prescriptionFile['error'] && $prescriptionFile['size'] < $this->maxFileSize) {
      return false;
    }
    $res = move_uploaded_file($tmpName, WWW_ROOT . Configure::read('PE.user_asset_path') . $newname);
    $this->loadModel('UserAsset');
    $this->UserAsset->save([
      'file_name' => $newname,
      'user_id' => $this->user['id'],
      'description' => $fileDesription
    ]);
    return $res;
  }
}
