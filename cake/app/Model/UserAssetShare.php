<?php
class UserAssetShare extends AppModel {

  public function getAssets($userId) {
    $userAssetShareJoinUserAsset = array(
      'table' => 'user_assets',
      'alias' => 'UserAsset',
      'type' => 'INNER',
      'conditions' => array('UserAssetShare.user_asset_id = UserAsset.id')
    );
    $userJoinUserAssetShare = array(
      'table' => 'users',
      'alias' => 'User',
      'type' => 'INNER',
      'conditions' => array('UserAsset.user_id = User.id')
    );
    $rolesJoinUser = array(
      'table' => 'roles',
      'alias' => 'Role',
      'type' => 'INNER',
      'conditions' => array('Role.id = User.role_id')
    );
    $prescriptionPath = Configure::read('PE.user_asset_path');
    $userAssets = $this->find('all', [
      'fields' => ['UserAsset.id', 'UserAsset.description', "concat('/$prescriptionPath', UserAsset.file_name) as link", 'UserAssetShare.created_at', "concat(User.name, '(', Role.name, ')') as shared_by"],
      'conditions' => ['UserAssetShare.user_id' => $userId, 'UserAssetShare.is_active' => 1],
      'joins' => [$userAssetShareJoinUserAsset, $userJoinUserAssetShare, $rolesJoinUser],
    ]);
    $allAssets = [];
    foreach ($userAssets as $userAsset) {
      $allAssets[] = [
        'id' => $userAsset['UserAsset']['id'],
        'description' => $userAsset['UserAsset']['description'],
        'link' => $userAsset[0]['link'],
        'created_at' => $userAsset['UserAssetShare']['created_at'],
        'shared_by'  => $userAsset[0]['shared_by'],
      ];
    }
    return $allAssets;
  }
}