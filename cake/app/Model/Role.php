<?php
class Role extends AppModel {

  public function getAllRoles() {
    return $this->find('list');
  }

  public function getActiveRole() {
    return $this->find('list', ['conditions' => ['is_active' => 1]]);
  }
  
}