<?php
class EmailVerificationKey extends AppModel {
  public $validate = [
    'key' => [
      'rule' => 'alphaNumeric',
      'allowEmpty' => false,
      'required' => 'create',
      'message' => 'Entered field is not a valid key'
    ],
    'user_id' => [
      'rule' => 'numeric',
      'allowEmpty' => false,
      'required' => 'create',
      'message' => 'Entered field is not a valid user_id'
    ],
  ];
}