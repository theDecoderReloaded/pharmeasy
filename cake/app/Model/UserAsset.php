<?php
class UserAsset extends AppModel {
  public $validate = [
    'file_name' => [
      'rule' => '/^[a-z0-9]+\.[a-z.]+$/',
      'allowEmpty' => false,
      'required' => 'create',
      'message' => 'Entered field is not a valid file_name'
    ],
    'user_id' => [
      'rule' => 'numeric',
      'allowEmpty' => false,
      'required' => 'create',
      'message' => 'Entered field is not a valid role_id'
    ],
  ];

  public function getAllAssetInfo($userId) {
    $userAssetShareJoinUserAsset = array(
      'table' => 'user_asset_shares',
      'alias' => 'UserAssetShare',
      'type' => 'LEFT',
      'conditions' => array('UserAssetShare.user_asset_id = UserAsset.id', 'UserAssetShare.is_active = 1')
    );
    $userJoinUserAssetShare = array(
      'table' => 'users',
      'alias' => 'User',
      'type' => 'LEFT',
      'conditions' => array('UserAssetShare.user_id = User.id')
    );
    $rolesJoinUser = array(
      'table' => 'roles',
      'alias' => 'Role',
      'type' => 'LEFT',
      'conditions' => array('Role.id = User.role_id')
    );
    $prescriptionPath = Configure::read('PE.user_asset_path');
    $userAssets = $this->find('all', [
      'fields' => ['UserAsset.id', 'UserAsset.description', "concat('/$prescriptionPath', UserAsset.file_name) as link", 'UserAsset.created_at', "group_concat(concat(User.name, '(', Role.name, ')') SEPARATOR ', ') as shared_users"],
      'conditions' => ['UserAsset.user_id' => $userId],
      'joins' => [$userAssetShareJoinUserAsset, $userJoinUserAssetShare, $rolesJoinUser],
      'group' => ['UserAsset.id']
    ]);
    // var_dump($userAssets);
    $allAssets = [];
    foreach ($userAssets as $userAsset) {
      $allAssets[] = [
        'id' => $userAsset['UserAsset']['id'],
        'description' => $userAsset['UserAsset']['description'],
        'link' => $userAsset[0]['link'],
        'created_at' => $userAsset['UserAsset']['created_at'],
        'shared_with'  => $userAsset[0]['shared_users'],
      ];
    }
    // var_dump($allAssets);
    return $allAssets;
  }
}