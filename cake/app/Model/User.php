<?php
class User extends AppModel {
  public $validate = [
    'email' => [
      'validEmailRule' => [
        'rule' => '/^[a-z0-9_\.-]+\@[\da-z\.-]+\.[a-z\.]{2,6}$/',
        'allowEmpty' => false,
        'required' => 'create',
        'message' => 'Entered field is not a valid email'
      ],
      'uniqueRule' => [
        'rule' => 'isUnique',
        'message' => 'Email id already in use'
      ]
    ],
    'password' => [
      'rule' => 'alphaNumeric',
      'allowEmpty' => false,
      'required' => 'create',
      'message' => 'Entered field is not a valid password'
    ],
    'role_id' => [
      'rule' => 'numeric',
      'allowEmpty' => false,
      'required' => 'create',
      'message' => 'Entered field is not a valid role_id'
    ],
  ];

  public function addUser($userInfo) {
    $this->clear();
    return $this->save($userInfo);
  }

  public function getUserInfo($email) {

  }
}